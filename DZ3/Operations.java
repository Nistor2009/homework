package проверенный;
public class Operations {
    double first;
    double second;

    public Operations(double first, double second) {
        this.first = first;
        this.second = second;
    }

    public double division(){
        return first / second;
    }

    public double multiplication(){
        return first * second;
    }

    public double addition(){
        return first + second;
    }

    public double subtraction(){
        return first - second;
    }

}
