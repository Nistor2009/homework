package проверенный;
/*
 * Так что я решил не загружать вас на этой(c)
 * я задолбался делать этот калькулятор ):
 */

/**
 * А кому легко :(
 */

public class Calculator {
    static double first;                       //первое значение
    static double second;                      //второе значение
    static Check check = new Check();          //класс для ввода и проверки значений

    public static void main(String[] args) {

        int chose;                          //выбор пользователя

        // Начальное меню
        menu();

        // Ввод пользователя, проверка введенного значения
        /** NOTE! Мне кажется chose можно сделать локальной переменной метода.
         * Она у тебя используется только в main. Так что нет смысла ее хранить.
         * Она и так будет жить до тех пор, пока выполняется метод main*/
        // согласен. Пусть будет в методе
        chose = check.checkInt();
        while (true) {
            /** NOTE! Мне кажется этот if можно упростить. У тебя значения от 0 до 4.*/
            //исправил
            if (!(chose >= 0 && chose <= 4)) {
                System.out.println("Данной операции не существует, повторите попытку");
                menu();
                chose = check.checkInt();
                /** NOTE! Если используешь фигурные скобки для if то лучше их добавлять
                 * и для else. А вообще лучше всегда добавлять фигурные скобки.
                 * Даже если у тебя там одна строчка кода.*/
                //исправил
            } else {
                break;
            }
        }

        // Проверка выхода
        if (chose == 0) {
            System.out.println("Программа завершает работу");
            return;
        }

        // Работа с выбранной операцией
        //Ввод значений
        first =  input("Введите первое число");
        second = input("Введите второе число");

        // Выполнение операции
        double result = 0;

        switch (chose) {
            case 1:
                result = new Operations(first, second).addition();
                break;
            case 2:
                result = new Operations(first, second).subtraction();
                break;
            case 3:
                result = new Operations(first, second).multiplication();
                break;
            //деление
            case 4:
                if (second == 0) {
                    boolean b = true;
                    while (b) {
                        System.out.println("Делить на 0 нельзя");
                        System.out.println("1: Ввести другое число\n" +
                                "2: Выйти в меню ");
                        chose = check.checkInt();
                        if (chose == 1) {
                            first =  input("Введите первое число");
                            second = input("Введите второе число");
                            if (second == 0) continue;
                            b = false;
                            result = new Operations(first, second).division();
                            System.out.println("Результат равен " + result);
                            check.close();
                        } else if (chose == 2) {
                            Calculator.main(new String[10]);
                            return;
                        }
                    }
                    return;
                } else result = new Operations(first, second).division();
                break;
        }

        System.out.println("Результат равен " + result);
        check.close();
    }

    public static void menu() {
        /** NOTE! Тут можно обойтись одним вызовом команды println*/
        //думал так нагляднее и проще выглядит, но исправил
        System.out.println("Выберите функцию\n" +
                "1: Сложение\n" +
                "2: Разность\n" +
                "3: Умножение\n" +
                "4: Деление\n" +
                "0: Выход");
    }

    public static double input(String s) {
        /** NOTE! Смотри. first и second можно сделать локальными переменными в main и
         * сделать input универсальным методом. То есть чтобы он просто считывал число
         * и возвращал его. Значение можно просто присвоить в нужыне переменные в main.
         * В качестве параметра этого метода можно передать строку, которая будет отображаться
         * как сообщение в консоли*/

//        System.out.println("Введите первое число");
//
//        // Ввод и проверка первого значения
//        first = check.checkDouble();
//
//        System.out.println("Введите второе число");
//
//        // Ввод и проверка второго значения
//        second = check.checkDouble();

        //переделал
        System.out.println(s);
       return check.checkDouble();

    }
}
