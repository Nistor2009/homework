package проверенный;
import java.util.Scanner;

public class Check {
    boolean b;
    Scanner scanner = new Scanner(System.in);

    public int checkInt() {
        b = false;
        int result;
        while (!b) {
            b = true;
            if (scanner.hasNextInt()) {
                result = scanner.nextInt();
                return result;
            } else {
                b = false;
                System.out.println("Введено неверное значение, повторите попытку.");
                scanner.next();
            }
        }
        return 0;
    }

    public double checkDouble() {
        b = false;
        double result;
        while (!b) {
            b = true;
            if (scanner.hasNextDouble()) {
                result = scanner.nextDouble();
                return result;
            } else {
                b = false;
                System.out.println("Введено неверное значение, повторите попытку.");
                scanner.next();
            }
        }
        return 0;
    }
    public void close(){
        scanner.close();
    }
}
