package DZ7;

import static java.lang.String.format;

/**
 * Дано натуральное число N. Нужно с помощью рекурсивного метода вывести в консоль числа от 0 до N;
 */
public class FirstTask1 {
    private static int i = 0;

    public static void recursion(int n) {
        System.out.print(i + " ");
        i++;
        if (i <= n) {
            recursion(n);
        } else {
            i = 0;
            System.out.println();
        }

    }

}

/**
 * Дано натуральное число N. Нужно с помощью рекурсивного метода вывести в консоль числа от 0 до N;
 */
public class FirstTask {

    private static String MESSAGE_FORMAT = "%d ";

    public static void recursion(int n) {
        recursion(0, n);
    }

    private static void recursion(int ind, int n) {
        System.out.print(format(MESSAGE_FORMAT, ind));
        if (ind < n) {
            recursion(++ind, n);
        }
    }
}