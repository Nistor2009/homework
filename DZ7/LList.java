/**
 * На 91-100* Реализовать свой собственный односвязный список и операцию добавления элемента
 * в список по индексу и операцию получения элемента списка по индексу.
 */
package DZ7;


public class LList<T> {

    /** NOTE! А почему тут статический индекс?*/

    /**
     * Когда писал, думал как бы показать, что это один индекс для всех Node, но счас понял, что если
     * создавать несколько классов LList, то это для них всех переменная index станет одной, и работать ничего правильно не будет. Исправляю
     */
//    private static int index = 0;
    private int index = 0;
    private Node<T> head = null;
    private Node<T> tail = null;

    // пустой ли список
    public boolean isEmpty() {
        return head == null;
    }

    // размер списка
    public int size() {
        return index;
    }

    // добавление элемента списка
    public void add(T data) {
        Node<T> node = new Node<>(data);
        if (isEmpty()) {
            head = node;
            tail = node;
            head.setIndex(index);
            tail.setIndex(index);
            index++;
        } else {
            tail.setNextNode(node);
            tail = node;
            node.setIndex(index);
            index++;
        }
    }

    // Добавление элемента списка по индексу
    public void add(T data, int index) {
        //чтобы не задали слишком большой индекс
        if (index > this.index) {
            return;
        }
        // добавление в конец списка
        if (index == this.index) {
            add(data);
            return;
        }

        // добавление в середину списка
        // создали новый элемент, установили ему идентификатор
        Node<T> setNode = new Node<>(data);
        setNode.setIndex(index);
        this.index++;

        Node<T> node = head;
        // если устанавливаем в начало
        if (index == 0) {
            setNode.setNextNode(head);
            head = setNode;
        } else{
            //Нашли элемент до устанавливаемого
            while (node.getIndex() != index - 1) {
                node = node.getNextNode();
            }
            //установили новые ссылки
            setNode.setNextNode(node.getNextNode());
            node.setNextNode(setNode);
        }

        // в оставшемся списке увеличиваем идентификаторы на 1
        node = setNode.getNextNode();
        node.setIndex(node.getIndex() + 1);
        while (node.getNextNode() != null) {
            node = node.getNextNode();
            node.setIndex(node.getIndex() + 1);
        }
    }

    // получение элемента списка по индексу
    public T get(int index) {
        Node<T> node = head;
        if (index >= size()) {
            return null;
        } else {
            while (node.getIndex() != index) {
                node = node.getNextNode();
            }
        }
        return node.getData();
    }


    private class Node<T> {
        private Node nextNode;
        private T data;
        private int index;

        public Node(T data) {
            this.data = data;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public Node getNextNode() {
            return nextNode;
        }

        public void setNextNode(Node nextNode) {
            this.nextNode = nextNode;
        }

        public T getData() {
            return data;
        }
    }

}
