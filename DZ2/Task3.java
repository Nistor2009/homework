/*
 * Дано целое число от 1 – 100. Необходимо найти максимальный делить числа
 * (кроме 1 и самого числа), деление на которое не дает остаток (т.е. деление без остатка);
 */
public class Task3 {
    public static void main(String[] args) {
        int number = 11;
        int del = 1;
        for (int i = 2; i <= number / 2; i++) {

            /**
             * NOTE! Мне кажется условие можно упростить
             * используя логическое И.
             */
            /**
             * исправляю
             */
//            if (number % i == 0) {
//                if (del < i) {
//                    del = i;
//                }
//            }
            if ((number % i == 0)&&(del < i)) {
                    del = i;
            }
        }
        if (del == 1) {
            System.out.println("Нет делителя кроме 1 и самого числа");
        } else System.out.println("Наибольший делитель числа " + number + " равен " + del);
    }
}
