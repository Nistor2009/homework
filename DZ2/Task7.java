/*Есть массив из десяти целых чисел.
 * Заменить каждый элемент с нечетным индексом на 0;
 */
public class Task7 {
    public static void main(String[] args) {
        int[] ints = {4, 11, 7, 43, 123, 76, 2, 5, 49, 10};
        System.out.print("Изначальный массив:           ");

        /** NOTE! немного странное форматирование кода для цмкла
         * Лучше в олну строчку
         * */
        for (int i : ints) {
            System.out.print(i + " ");
        }
        System.out.println();
        for (int i = 1; i < ints.length; i++) {
            if (i % 2 != 0) {
                ints[i] = 0;
            }
        }
        System.out.print("Нечетный индекс заменен на 0: ");

        /** NOTE! немного странное форматирование кода для цмкла
         * Лучше в олну строчку
         * */
        for (int i : ints) {
            System.out.print(i + " ");
        }
    }
}
