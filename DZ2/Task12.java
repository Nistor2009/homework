/* Есть массив чисел с плавающей запятой.
 * Необходимо реализовать сортировку методом выбора по возрастанию и по убыванию
 */
public class Task12 {
    public static void main(String[] args) {
        double[] array = {65.4, 5.7, 2.0, 9.1, 3.4, 3.3, 11.2};
        System.out.print("Изначальный массив:   ");
        for (double d : array) {
            System.out.print(d + "  ");
        }
        System.out.println();

        //реализация сортировки выбором по возрастанию
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] > array[j]) {
                    double d = array[i];
                    array[i] = array[j];
                    array[j] = d;
                }
            }
        }

        System.out.print("Массив по возрастанию:");
        for (double d : array) {
            System.out.print(d + "  ");
        }
        System.out.println();

        //реализация сортировки выбором по убыванию
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] < array[j]) {
                    double d = array[i];
                    array[i] = array[j];
                    array[j] = d;
                }
            }
        }

        System.out.print("Массив по убыванию:   ");
        /** NOTE! немного странное форматирование кода для цмкла
         * Лучше в одну строчку
         * */
        for (double d : array) {
            System.out.print(d + "  ");
        }
    }
}
