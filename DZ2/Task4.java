/*Есть массив из десяти целых чисел.
 * Необходимо вывести максимально и минимальное число из массива
 */
public class Task4 {
    public static void main(String[] args) {
        int[] ints = {4, 11, 7, 43, 123, 76, 2, 5, 49, 10};
        for (int i = 0; i < ints.length - 1; i++) {
            for (int j = i + 1; j < ints.length; j++) {
                if (ints[i] > ints[j]) {
                    int k = ints[i];
                    ints[i] = ints[j];
                    ints[j] = k;
                }
            }
        }
        System.out.println("Максимальное число = " + ints[ints.length - 1]);
        System.out.println("Минимальное число = " + ints[0]);
    }
}
