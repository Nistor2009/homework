/*Есть массив из десяти целых чисел.
 * Необходимо заменить каждый четный элемент массива на его удвоенное произведение (т.е. возвести квадрат).
 * Например, есть массив [1, 2, 3,4], тогда результатом должен быть [1, 4, 3, 16];
 */
public class Task6 {
    public static void main(String[] args) {
        int[] ints = {4, 11, 7, 43, 123, 76, 2, 5, 49, 10};
        System.out.print("Изначальный массив:       ");
        for (int i : ints) {
            System.out.print(i + " ");
        }
        System.out.println();
        for (int i = 0; i < ints.length; i++) {
            if (ints[i] % 2 == 0) {
                ints[i] *= ints[i];
            }
        }
        System.out.print("Четные числа в квадрате: ");
        /** NOTE! немного странное форматирование кода для цмкла*/

        /**
         * мне так idea поставила автоматом. Не стал ее исправлять. p.s. в остальных заданиях просто исправил
         */
//        for (int i :
//                ints) {
//            System.out.print(i + " ");
//        }
        for (int i : ints) {
            System.out.print(i + " ");
        }
    }
}
