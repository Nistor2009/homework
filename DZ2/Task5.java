/*Есть массив из десяти целых чисел.
 *Необходимо поменять элементы массива в обратном порядке
 */
public class Task5 {
    public static void main(String[] args) {
        /**
         * NOTE! Этот способо работает, но на самом деле
         * есть более оптимальный подход с использование только одного массива.
         * По сути бежишь по массиву, одновременно с двух сторон и
         * меняешь элементы местами через переменную.
         * Пределом цикла будет середина массива.
         */

        /**
         * исправляю
         */
        int[] ints = {4, 11, 7, 43, 123, 76, 2, 5, 49, 10};
//        int[] result = new int[ints.length];
//        int a = 0;
//        for (int i = ints.length - 1; i >= 0; i--) {
//            result[a] = ints[i];
//            a++;
//        }
        System.out.print("Изначальный массив: ");
        for (int i : ints) {
            System.out.print(i + " ");
        }
        System.out.println();

        /**
         * новое решение
         */

        for (int i = 0; i < ints.length / 2; i++) {
            int j = ints[i];
            ints[i] = ints[ints.length - 1 - i];
            ints[ints.length - 1 - i] = j;
        }

        System.out.print("Обратный массив: ");
        for (int i : ints) {
            System.out.print(i + " ");
        }
    }
}
