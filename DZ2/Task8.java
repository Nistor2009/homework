/*Есть массив из десяти целых чисел.
 *Необходимо посчитать среднее арифметическое для всех элементов массива
 */
public class Task8 {
    public static void main(String[] args) {
        int[] ints = {4, 11, 7, 43, 123, 76, 2, 5, 49, 10};
        int sum = 0;
        for (int i = 0; i < ints.length; i++) {
            sum += ints[i];
        }
        int average = sum / ints.length;
        System.out.println("Среднее арифметическое чисел массива: " + average);
    }
}
