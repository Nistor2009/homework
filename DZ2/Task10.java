/*Есть двумерный массив (MxN) целых чисел (от 0 до 9).
 * С помощью команд System.out.println и System.out.print вывести в консоль
 * весь массив как в примере ниже. По возможности использовать чикл for-each;
 *    1 2 4 5
 *    8 9 7 0
 *    6 6 4 3
 */
public class Task10 {
    public static void main(String[] args) {
        int[][] ints = {{1, 2, 4, 5},
                {8, 9, 7, 0},
                {6, 6, 4, 3}};
        for (int i = 0; i < ints.length; i++) {
            /** NOTE! немного странное форматирование кода для цмкла
             * Лучше в олну строчку
             * */
            for (int j : ints[i]) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
    }
}
