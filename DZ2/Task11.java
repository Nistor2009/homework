/* Есть массив чисел с плавающей запятой.
 * Необходимо реализовать сортировку пузырьком по возрастанию и по убыванию
 */
public class Task11 {
    public static void main(String[] args) {
        double[] array = {65.4, 5.7, 2.0, 9.1, 3.4, 3.3, 11.2};
        System.out.print("Изначальный массив:   ");

        /** NOTE! немного странное форматирование кода для цмкла
         * Лучше в олну строчку
         * */
        for (double d : array) {
            System.out.print(d + "  ");
        }
        System.out.println();

        //реализация сортировки пузырьком по возрастанию
        boolean b = false;
        while (!b) {
            b = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    b = false;
                    double d = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = d;
                }
            }
        }

        System.out.print("Массив по возрастанию:");
        for (double d : array) {
            System.out.print(d + "  ");
        }
        System.out.println();

        //реализация сортировки пузырьком по убыванию
        b = false;
        while (!b) {
            b = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] < array[i + 1]) {
                    b = false;
                    double d = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = d;
                }
            }
        }

        System.out.print("Массив по убыванию:   ");
        for (double d : array) {
            System.out.print(d + "  ");
        }
    }
}
