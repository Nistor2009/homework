/*
 * Посчитать степень числа. Использовать цикл for;
 */
public class Task2 {
    public static void main(String[] args) {
        int number = 2;
        int ex = 4;
        int result = 1;
        for (int i = 1; i <= ex; i++) {
            result *= number;
        }
        System.out.println("Число " + number + " в степени " + ex + " = " + result);
    }
}
