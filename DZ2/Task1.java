/*
 *Посчитать сумму всех чисел от 0 до N. Реализовать задание через while;
 */

public class Task1 {
    public static void main(String[] args) {
        int i = 1;
        int n = 8;
        int sum = 0;
        while (i <= n) {
            sum += i;
            i++;
        }
        System.out.println("Сумма чисел от 0 до " + n + " = " + sum);
    }
}
