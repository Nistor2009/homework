/*
 * Получить первые N чисел из ряда Фибоначчи (использовать только циклы);
 */
public class Task9 {
    public static void main(String[] args) {
        int n = 10;
        int[] i = new int[n];
        i[0] = 0;
        i[1] = 1;
        for (int j = 2; j < i.length; j++) {
            i[j] = i[j - 1] + i[j - 2];
        }
        System.out.print("Первые " + n + " чисел из ряда Фибоначчи:");
        for (int j = 0; j < i.length; j++) {
            System.out.print(" " + i[j]);
        }
    }
}
