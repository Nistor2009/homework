package Sorting;

public class SelectionSorting implements Sortable {
    @Override
    public int[] sort(int[] arrayForSorting, boolean ascending) {
        for (int i = 0; i < arrayForSorting.length - 1; i++) {
            for (int j = i + 1; j < arrayForSorting.length; j++) {
                boolean condition;
                if (ascending) {
                    condition = arrayForSorting[i] > arrayForSorting[j];
                } else {
                    condition = arrayForSorting[i] < arrayForSorting[j];
                }
                if (condition) {
                    int buf = arrayForSorting[i];
                    arrayForSorting[i] = arrayForSorting[j];
                    arrayForSorting[j] = buf;
                }
            }
        }
        return arrayForSorting;
    }


}
