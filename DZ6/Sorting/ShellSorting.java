package Sorting;

public class ShellSorting implements Sortable {
    @Override
    public int[] sort(int[] arrayForSorting, boolean ascending) {
        int h = 1;
        while (h < arrayForSorting.length / 3) {
            h = 3 * h + 1;
        }
        /**
         * по алгоритму сортировки что-то вроде понял, но пока как-то смутно
         */
        while (h >= 1) {
            for (int i = h; i < arrayForSorting.length; i++) {
                for (int j = i; j >= h; j -= h) {
                    boolean condition;
                    if (ascending) {
                        condition = arrayForSorting[j] < arrayForSorting[j - h];
                    } else {
                        condition = arrayForSorting[j] > arrayForSorting[j - h];
                    }
                    if (condition) {
                        int buf = arrayForSorting[j];
                        arrayForSorting[j] = arrayForSorting[j - h];
                        arrayForSorting[j - h] = buf;
                    }
                }
            }
            h = h / 3;
        }
        return arrayForSorting;
    }


}
