package Sorting;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProgrammeLogic {
    private static final int EXITFLAG = 0;
    private static final int BUBBLE_SORT = 1;
    private static final int SELECTION_SORT = 2;
    private static final int SHELL_SORT = 3;

    public void startProgramm(){
        boolean finish = false;
        while (!finish){
            finish = true;
            mainMenu();
            System.out.println();
            String message = "Ввести другой массив?\n1 - да\n2 - нет";
            if(booleanChoose(message)){
                finish = false;
            }
        }
    }

    private void mainMenu() {
        Scanner scanner = new Scanner(System.in);
        String firstMessage = "Введите массив чисел";
        System.out.println(firstMessage);
        String array = scanner.nextLine();

        //получаем массив чисел из строки
        String[] stringArray = array.split(" ");
        int[] arrayForSorting = new int[stringArray.length];

        for (int i = 0; i < stringArray.length; i++) {
            Pattern pattern = Pattern.compile("-?\\d+");
            Matcher matcher = pattern.matcher(stringArray[i]);
            if (matcher.find()) {
                arrayForSorting[i] = Integer.parseInt(matcher.group());
            }
        }


        // next menu
        String secondMessage = BUBBLE_SORT + " – Сортировка пузырьком\n" + SELECTION_SORT + " - Сортировка методом выбора\n" + SHELL_SORT + " – Сортировка методом Шелла\n" + EXITFLAG + " – Выход";
        int choise = choose(secondMessage, 0, 4);     // выбор пользователя (сообщение, нижняя(входит) и верхняя(не входит) границы)

        boolean ascending = true;       // возрастание или убывание

        String thirdMessage = "1 - по возрастанию; 2 - по убыванию";

        /** NOTE! У тебя здесь куча мест с одентичной логикой
         * (печать сортированного массива). Лучше ее вынести в отдельный метод
         * и просто вызывать его в нужных местах*/
        switch (choise) {
            case EXITFLAG:
                System.out.println("Программа завершена");
                System.exit(0);
            case BUBBLE_SORT:
                System.out.println("Сортировка массива пузырьком:");
                ascending = booleanChoose(thirdMessage);
                printArray(SortingClass.createBubbleSort().sort(arrayForSorting, ascending));
                break;
            case SELECTION_SORT:
                System.out.println("Сортировка массива методом выбора");
                ascending = booleanChoose(thirdMessage);
                printArray(SortingClass.createSelectionSorting().sort(arrayForSorting, ascending));
                break;
            case SHELL_SORT:
                System.out.println("Сортировка массива методом Шелла");
                ascending = booleanChoose(thirdMessage);
                printArray(SortingClass.createShellSorting().sort(arrayForSorting, ascending));
                break;
        }

    }
    private void printArray(int[] array){
        for (int i : array) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    // проверка, что введено число
    private int checkInputValue() {
        Scanner scanner = new Scanner(System.in);
        boolean exitFlag = false;
        int returnValue = 0;
        while (!exitFlag) {
            exitFlag = true;
            if (scanner.hasNextInt()) {
                returnValue = scanner.nextInt();
            } else {
                exitFlag = false;
                System.out.println("Введите число");
                scanner.next();
            }
        }
        return returnValue;
    }

    // проверка, что введенное число в нужном диапазоне
    private int choose(String message, int bottom, int top) {
        int choice;     // значение от пользователя
        do {
            System.out.println(message);
            choice = checkInputValue();
        }
        while (!(choice >= bottom && choice < top));
        return choice;
    }

    private boolean booleanChoose(String message) {
        int choise = choose(message, 1, 3);
        if (choise == 1) {
            return true;
        } else {
            return false;
        }
    }
}
