package Sorting;

public class SortingClass {
    public static Sortable createBubbleSort(){
        return new BubbleSort();
    }
    public static Sortable createSelectionSorting(){
        return new SelectionSorting();
    }

    public static Sortable createShellSorting(){
        return new ShellSorting();
    }
}
