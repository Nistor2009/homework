package Sorting;

public interface Sortable {
    int[] sort(int[] arrayForSorting,boolean ascending);
}
