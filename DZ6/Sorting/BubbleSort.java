package Sorting;

public class BubbleSort implements Sortable {

    @Override
    public int[] sort(int[] arrayForSorting,boolean ascending) {
        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < arrayForSorting.length - 1; i++) {
                boolean condition;
                if(ascending){
                    condition = arrayForSorting[i] > arrayForSorting[i + 1];
                } else {
                    condition = arrayForSorting[i] < arrayForSorting[i + 1];
                }
                if (condition) {
                    isSorted = false;
                    int buf = arrayForSorting[i];
                    arrayForSorting[i] = arrayForSorting[i + 1];
                    arrayForSorting[i + 1] = buf;
                }
            }
        }
        return arrayForSorting;
    }


}
