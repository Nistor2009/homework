package converter;

public interface Converner {
    double convert(double temp);
}
