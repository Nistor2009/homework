package converter;

public class ConvertToKelvin implements Converner {
    @Override
    public double convert(double temp) {
        return temp + 273.15;
    }
}
