package converter;

import java.util.Scanner;

public class ProgrammeLogic {
    private static final int EXITFLAG = 0;
    private static final int FARENGATE = 1;
    private static final int KELVIN = 2;
    public void startProgramme() {
        boolean exit = false;
        while (!exit) {
            exit = true;
            mainMenu();
            String exitMenu = "Желаете продолжить перевод?\n1 - да\n2 - нет";
            int userChoice = choose(exitMenu, 1, 3);
            if (userChoice == 1) {
                exit = false;
            }
        }
    }

    private void mainMenu() {
        System.out.println("Введите температуру в градусах Цельсия(через запятую)");
        double temp = checkInputValue();
        double convertTemp = 0;
        String secondMenu = "Перевод температуры\n" + FARENGATE + " - в Фаренгейты\n" + KELVIN + " - в Кельвины\n" + EXITFLAG + " - выход";
        int userChoice = choose(secondMenu, 0, 3);

        /** NOTE! По идее System.out.println(convertTemp);
         * можно перенести в конец метода. Тогда у тебя уйдет лишний код */
        switch (userChoice) {
            case EXITFLAG:
                System.out.println("Программа завершена");
                System.exit(0);
            case FARENGATE:
                System.out.print("Перевод в Фаренгейты: ");
               convertTemp = ConverterClass.createConverterToFarengate().convert(temp);
                break;
            case KELVIN:
                System.out.print("Перевод в Кельвины: ");
                convertTemp = ConverterClass.createConverterToKelvin().convert(temp);
                break;
        }
        System.out.println(convertTemp);
    }

    // проверка, что введено число
    private double checkInputValue() {
        Scanner scanner = new Scanner(System.in);
        boolean exitFlag = false;
        double returnValue = 0;
        while (!exitFlag) {
            exitFlag = true;
            if (scanner.hasNextDouble()) {
                returnValue = scanner.nextDouble();
            } else {
                exitFlag = false;
                System.out.println("Введите число");
                scanner.next();
            }
        }
        return returnValue;
    }

    // проверка, что введенное число в нужном диапазоне
    private int choose(String message, int bottom, int top) {
        int choice;     // значение от пользователя
        do {
            System.out.println(message);
            choice = (int) checkInputValue();
        }
        while (!(choice >= bottom && choice < top));
        return choice;
    }
}
