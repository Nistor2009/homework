package converter;

public class ConvertToFarengate implements Converner {
    @Override
    public double convert(double temp) {
        return temp * 9 / 5 + 32;
    }
}
