package web;

public class LaserHomePrinter extends Printable {
    @Override
    public void print(String message) {
        System.out.println("Вывод сообщения через лазерный домашний принтер:\n" + message);
    }
}
