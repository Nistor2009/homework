package web;

public class HomePrinter extends Printable {
    @Override
    public void print(String message) {
        System.out.println("Вывод сообщения через домашний принтер:\n" + message);
    }
}
