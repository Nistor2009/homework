package web;

import java.util.Scanner;

public class ProgrammeLogic {
    private static final int EXITFLAG = 0;
    private Printable[] availableDeviсes = {new Plotter(), new HomePrinter(), new LaserHomePrinter(), new OutputScreen(), new _3DPrinter()};

    public void startProgramme() {
        Printable printable = printerPick();    //выбираем принтер
        printMessage(printable);                //печатаем сообщение

        boolean isExit = false;
        // продолжение программы
        while (!isExit) {
            isExit = true;
            String message = "Продолжить печать или выбрать другое устройство?\n1 - продолжить\n2 - выбрать устройство\n" + EXITFLAG + " - выход";
            int userChoice = choose(message, 0, 3);
            switch (userChoice) {
                case EXITFLAG:
                    System.out.println("Программа печати завершена");
                    break;
                case 1:
                    isExit = false;
                    printMessage(printable);
                    break;
                case 2:
                    isExit = false;
                    printable = printerPick();
                    printMessage(printable);
                    break;
            }
        }
    }
    private Printable printerPick(){
        System.out.println("Доступные устройства:");
        for (int i = 0; i < availableDeviсes.length; i++) {
            System.out.println((i + 1) + " - " + availableDeviсes[i].toString());
        }
        System.out.println("0 - выход");
        String secondMessage = "Выберите устройство для печати по номеру";

        int userChoice = choose(secondMessage, 0, availableDeviсes.length + 1);
        Printable printable = null;

        /** NOTE! А зачем создавать новый объект, если он у тебя и
         * так хранится в availableDeviсes?
         */
        switch (userChoice) {
            case EXITFLAG:
                System.out.println("Программа печати завершена");
                System.exit(0);
            case 1:
                printable = availableDeviсes[0];
                break;
            case 2:
                printable = availableDeviсes[1];
                break;
            case 3:
                printable = availableDeviсes[2];
                break;
            case 4:
                printable = availableDeviсes[3];
                break;
            case 5:
                printable = availableDeviсes[4];
                break;
        }
        return printable;
    }

    private void printMessage(Printable printable) {
        // вывод доступных устройств
        System.out.println("Введите сообщение для печати:");
        Scanner scanner = new Scanner(System.in);
        String message = scanner.nextLine();

        printable.print(message);
    }

    // проверка, что введено число
    private int checkInputValue() {
        Scanner scanner = new Scanner(System.in);
        boolean exitFlag = false;
        int returnValue = 0;
        while (!exitFlag) {
            exitFlag = true;
            if (scanner.hasNextInt()) {
                returnValue = scanner.nextInt();
            } else {
                exitFlag = false;
                System.out.println("Введите число");
                scanner.next();
            }
        }
        return returnValue;
    }

    // проверка, что введенное число в нужном диапазоне
    private int choose(String message, int bottom, int top) {
        int choice;     // значение от пользователя
        do {
            System.out.println(message);
            choice = checkInputValue();
        }
        while (!(choice >= bottom && choice < top));
        return choice;
    }
}
