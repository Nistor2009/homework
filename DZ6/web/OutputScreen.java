package web;

public class OutputScreen extends Printable {
    @Override
    public void print(String message) {
        System.out.println("Вывод сообщения через наружный экран:\n" + message);
    }
}
