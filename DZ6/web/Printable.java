package web;

public abstract class Printable {
    abstract void print(String message);

    @Override
    public String toString(){
        return this.getClass().getSimpleName();
    }
}
