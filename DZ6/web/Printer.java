package web;

public class Printer {
    public static Printable create3DPrinter(){
        return new _3DPrinter();
    }
    public static Printable createHomePrinter(){
        return new HomePrinter();
    }
    public static Printable createLaserHomePrinter(){
        return new LaserHomePrinter();
    }
    public static Printable createOutputScreen(){
        return new OutputScreen();
    }
    public static Printable createPlotter(){
        return new Plotter();
    }

}
