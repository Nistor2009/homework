package menu;

import menu.item.MainMenuAction;
import menu.item.MenuItem;

import java.util.LinkedList;
import java.util.List;

import static menu.MainMenuTextConst.*;

public class MenuFactory {

    /**
     * возвращает список пунктов меню из MainMenuAction
     */

    public static List<MenuItem> getMainMenu(MainMenuAction... menuActions) {
        LinkedList<MenuItem> linkedList = new LinkedList<>();
        for (int i = 0; i < menuActions.length; i++) {
            MenuItem menuItem = getMainMenuItem(i, menuActions[i]);
            if (menuItem != null) {
                linkedList.add(menuItem);
            }
        }
        return linkedList;
    }

    /**
     * при добавлении элемента сюда тоже добавить пункт выбора
     */

    private static MenuItem getMainMenuItem(int ind, MainMenuAction mainMenuAction) {
        switch (mainMenuAction) {
            case READ_FILE:
                return new MenuItem(ind, ITEM_TEXT_READ_FILES);
            case ADD_FILE:
                return new MenuItem(ind, ITEM_TEXT_ADD_FILE);
            case DELETE_FILE:
                return new MenuItem(ind, DELETE_FILE_MESSAGE);
            case ADD_ENTRY:
                return new MenuItem(ind, ADD_ENTRY_MESSAGE);
            case ZIP_FILES:
                return new MenuItem(ind, ZIP_FILES_MESSAGE);
            case UNZIP_FILES:
                return new MenuItem(ind, UNZIP_FILES_MESSAGE);
            case EXIT:
                return new MenuItem(ind, ITEM_TEXT_EXIT);
        }
        return null;
    }

    private MenuFactory() {
    }
}
