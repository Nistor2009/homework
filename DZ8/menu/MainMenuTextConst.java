package menu;

public class MainMenuTextConst {

    static final String ITEM_TEXT_READ_FILES = "Прочитать файлы";
    static final String ITEM_TEXT_EXIT = "Выход";
    static final String ITEM_TEXT_ADD_FILE = "Добавить файл";
    public static final String ITEM_WITH_NUMBER = "%d - %s";
    public static final String ACTION_ITEM_ERROR_MESSAGE = "Пункт меню не распознан";
    public static final String READ_FILE_ERROR_MESSAGE = "Файл не прочитан";
    public static final String WRITE_FILE_ERROR_MESSAGE = "Файл не записан";
    public static final String DELETE_FILE_MESSAGE = "Удалить файл";
    public static final String ADD_ENTRY_MESSAGE = "Добавить запись в файл";
    public static final String ZIP_FILES_MESSAGE = "Создать резервную копию файлов";
    public static final String UNZIP_FILES_MESSAGE = "Восстановление резервной копии файлов";

    private MainMenuTextConst(){
    }
}
