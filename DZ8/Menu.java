import log.LoggerClass;
import exception.MenuActionException;
import menu.item.MainMenuAction;
import menu.item.MenuItem;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

import static menu.MainMenuTextConst.*;
import static menu.MenuFactory.getMainMenu;

class Menu {
    private Logger logger;
    private FileManager fileManager;
    private Scanner scanner;

    Menu(String rootDirPath) {
        this.fileManager = new FileManager(rootDirPath);
        this.scanner = new Scanner(System.in);
        logger = LoggerClass.createLogger();
    }

    void start() {
        while (true) {
            // Печатаем пункты меню
            printMenuItems(getMainMenu(MainMenuAction.values()));
            try {
                int menuNumber = readIntFromConsole();
                MainMenuAction mainMenuAction = MainMenuAction.getMainMenuAction(menuNumber);
                mainMenuAction(mainMenuAction);
            } catch (MenuActionException e) {
                e.printStackTrace();
                printText(ACTION_ITEM_ERROR_MESSAGE);
            }
        }
    }

    private void mainMenuAction(MainMenuAction mainMenuAction) {
        switch (mainMenuAction) {
            case READ_FILE:
                workWithFiles();
                break;
            case DELETE_FILE:
                deleteFile();
                break;
            case ADD_FILE:
                addNewFileAction();
                break;
            case ADD_ENTRY:
                addEntryToFile();
                break;
            case ZIP_FILES:
                zipFiles();
                break;
            case UNZIP_FILES:
                unzipFiles();
                break;
            case EXIT:
                System.exit(0);
        }

    }

    private void zipFiles(){
        try {
            fileManager.zippingFiles();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Файлы заархивированны");
        logger.info("Zip files");
    }

    private void unzipFiles(){
        try {
            fileManager.unzipFiles();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Файлы разархивированны");
        logger.info("Unzip files");
    }

    private void deleteFile() {
        System.out.println("Введите имя удаляемого файла файла:");
        String fileName = readStringFromConsole();
        fileManager.deleteFile(fileName);
        logger.info("Deleted file \"" + fileName + "\"");

    }

    private void addEntryToFile(){
        System.out.println("Введите имя файла, в который нужно добавить данные:");
        String fileName = readStringFromConsole();
        System.out.println("Введите текст до слова *exit:");
        String fileText = readStringLines();
        try {
            fileManager.addEntryToFile(fileName, fileText);
            logger.info("Edited file \"" + fileName + "\"");
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    private void addNewFileAction() {
        System.out.println("Введите имя файла:");
        String fileName = readStringFromConsole();
        System.out.println("Введите текст до слова *exit:");
        String fileText = readStringLines();

        try {
            fileManager.writeFile(fileName, fileText);
            logger.info("Added file \"" + fileName + "\"");

        } catch (IOException e) {
            e.printStackTrace();
            printText(WRITE_FILE_ERROR_MESSAGE);
        }
    }

    private void workWithFiles() {
        List<File> files = fileManager.getFileList();
        printFiles(files);
        File file = files.get(readIntFromConsole());
        logger.info("Viewed file \"" + file.getName() + "\"");

        String text;
        try {
            text = fileManager.readFile(file);
        } catch (IOException e) {
            e.printStackTrace();
            text = READ_FILE_ERROR_MESSAGE;
        }
        printText(text);
    }

    private <T> void printText(T text) {
        System.out.println(text.toString());
    }

    private void printFiles(List<File> files) {
        for (int i = 0; i < files.size(); i++) {
            File file = files.get(i);
            printText(String.format(ITEM_WITH_NUMBER, i, file.getName()));
        }
    }

    private void printMenuItems(List<MenuItem> menuItems) {
        for (MenuItem menuItem : menuItems) {
            printText(menuItem);
        }
    }

    private int readIntFromConsole() {
        int number = scanner.nextInt();
        scanner.nextLine();
        return number;
    }

    private String readStringFromConsole() {
        return scanner.nextLine();
    }

    private String readStringLines() {
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = readStringFromConsole()) != null && !line.equals("*exit")) {
            stringBuilder.append(line).append("\n");
        }
        return stringBuilder.toString();
    }
}
