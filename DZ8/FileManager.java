import consts.Consts;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;


/**
 * Принимает в конструктор директорию.
 * Считывает имена файлов из директории
 * По номеру файла считывает сам файл
 * По имени и тексту записывает новый файл
 */

class FileManager {

    private static int compare(File file1, File file2) {
        return file1.getName().compareTo(file2.getName());
    }

    private File rootDirectory;

    FileManager(String rootDirPath) {
        this.rootDirectory = new File(rootDirPath);
    }

    void unzipFiles() throws IOException {
        byte[] buffer = new byte[1024];
        File directory = new File(Consts.ROOT_DIR_PATH);
        ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(Consts.ZIP_FILES_PATH));
        ZipEntry zipEntry = zipInputStream.getNextEntry();
        while (zipEntry != null){
            File file = new File(directory,zipEntry.getName());
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            int length;
            while ((length = zipInputStream.read(buffer)) > 0){
                fileOutputStream.write(buffer,0,length);
            }
            fileOutputStream.close();
            zipEntry = zipInputStream.getNextEntry();
        }
        zipInputStream.closeEntry();
        zipInputStream.close();
    }

    void zippingFiles() throws IOException {

        List<File> fileList = getFileList();
        FileOutputStream fos = new FileOutputStream(Consts.ZIP_FILES_PATH);
        ZipOutputStream zos = new ZipOutputStream(fos);

        for (File file : fileList) {
            FileInputStream fis = new FileInputStream(file);
            ZipEntry zipEntry = new ZipEntry(file.getName());
            zos.putNextEntry(zipEntry);
            byte[] bytes = new byte[1024];
            int length;
            while ((length = fis.read(bytes)) >= 0) {
                zos.write(bytes, 0, length);
            }
            fis.close();
        }
        zos.close();
        fos.close();
    }

    List<File> getFileList() {
        File[] fileArray = rootDirectory.listFiles();
        if (fileArray != null) {
            Arrays.sort(fileArray, FileManager::compare);
        }
        return fileArray != null ? asList(fileArray) : emptyList();
    }

    String readFile(File file) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                stringBuilder.append(line)
                        .append("\n");
            }
        }
        return stringBuilder.toString();
    }

    void writeFile(String fileName, String fileText) throws IOException {
        File file = new File(rootDirectory, fileName);
        if (!file.exists()) {
            file.createNewFile();
        }

        try (FileWriter fileWriter = new FileWriter(file);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            bufferedWriter.write(fileText);
        }
    }

    void deleteFile(String fileName) {
        File file = new File(rootDirectory, fileName);
        if (file.delete()) {
            System.out.println(fileName + " файл удален");
        } else System.out.println(fileName + " не обнаружено");
    }

    void addEntryToFile(String fileName, String fileText) throws IOException {
        File file = new File(rootDirectory, fileName);
        try (FileWriter fileWriter = new FileWriter(file, true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            bufferedWriter.write(fileText);
        }
        ;
    }
}
