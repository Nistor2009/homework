package log;

import consts.Consts;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.*;

public class LoggerClass {
    private LoggerClass() {

    }


    public static Logger createLogger() {
        SimpleDateFormat formatForDate = new SimpleDateFormat("[yyyy/MM/dd hh:mm:ss]");

        Logger logger = Logger.getLogger(LoggerClass.class.getName());

        Handler fileHandler;
        Handler consoleHandler;

        logger.setUseParentHandlers(false);

            try {
                fileHandler = new FileHandler(Consts.FILE_HANDLER_PATH);
                fileHandler.setFormatter(new Formatter() {
                    @Override
                    public String format(LogRecord record) {
                        return formatForDate.format(new Date()) + " - " + record.getMessage() + "\n";
                    }
                });
                logger.addHandler(fileHandler);

            } catch (IOException e) {
                e.printStackTrace();
            }

        return logger;
    }



}


