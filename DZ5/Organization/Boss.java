package Organization;

public class Boss extends Executive {
    private Worker[] inSubmission;

    public Worker[] getInSubmission() {
        return inSubmission;
    }

    public void setInSubmission(Worker[] inSubmission) {
        this.inSubmission = inSubmission;
    }
}
