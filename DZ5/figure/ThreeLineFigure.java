package figure;

public class ThreeLineFigure extends GeometricFigure {
    protected int firstLine;
    protected int secondLine;
    protected int thirdLine;

    public int getFirstLine() {
        return firstLine;
    }

    public void setFirstLine(int firstLine) {
        this.firstLine = firstLine;
    }

    public int getSecondLine() {
        return secondLine;
    }

    public void setSecondLine(int secondLine) {
        this.secondLine = secondLine;
    }

    public int getThirdLine() {
        return thirdLine;
    }

    public void setThirdLine(int thirdLine) {
        this.thirdLine = thirdLine;
    }
}
