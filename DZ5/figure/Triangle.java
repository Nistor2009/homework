package figure;

public class Triangle extends ThreeLineFigure {
    private double firstCorner;
    private double secondCorner;
    private double thirdCorner;


    public double getFirstCorner() {
        return firstCorner;
    }

    public void setFirstCorner(double firstCorner) {
        this.firstCorner = firstCorner;
    }

    public double getSecondCorner() {
        return secondCorner;
    }

    public void setSecondCorner(double secondCorner) {
        this.secondCorner = secondCorner;
    }

    public double getThirdCorner() {
        return thirdCorner;
    }

    public void setThirdCorner(double thirdCorner) {
        this.thirdCorner = thirdCorner;
    }

}
