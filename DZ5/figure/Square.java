package figure;

public class Square extends FourLineFigure {
    private int line;

    public int getLine() {
        return line;
    }

    public void setLine(int line){
        this.line = line;
        firstLine = this.line;
        secondLine = this.line;
        thirdLine = this.line;
        fourthLine = this.line;
    }

    @Override
    public int getFirstLine() {
        return getLine();
    }

    @Override
    public void setFirstLine(int firstLine) {
        setLine(firstLine);
    }

    @Override
    public int getSecondLine() {
        return getLine();
    }

    @Override
    public void setSecondLine(int secondLine) {
        setLine(secondLine);
    }

    @Override
    public int getThirdLine() {
        return getLine();
    }

    @Override
    public void setThirdLine(int thirdLine) {
        setLine(thirdLine);
    }

    @Override
    public int getFourthLine() {
        return getLine();
    }

    @Override
    public void setFourthLine(int fourthLine) {
        setLine(fourthLine);
    }
}
