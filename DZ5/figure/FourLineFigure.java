package figure;

public class FourLineFigure extends GeometricFigure {
    protected int firstLine;
    protected int secondLine;

    public int getFirstLine() {
        return firstLine;
    }

    public void setFirstLine(int firstLine) {
        this.firstLine = firstLine;
    }

    public int getSecondLine() {
        return secondLine;
    }

    public void setSecondLine(int secondLine) {
        this.secondLine = secondLine;
    }

    public int getThirdLine() {
        return thirdLine;
    }

    public void setThirdLine(int thirdLine) {
        this.thirdLine = thirdLine;
    }

    public int getFourthLine() {
        return fourthLine;
    }

    public void setFourthLine(int fourthLine) {
        this.fourthLine = fourthLine;
    }

    protected int thirdLine;
    protected int fourthLine;
}
