package AutoСatalog.Cars.Audi;

public class AudiQ5 extends Audi {
    private final int milage = 190_000;
    private final int issueYear = 2009;
    private final String color = "черный";


    @Override
    protected int setMilage() {
        return milage;
    }

    @Override
    protected int setIssueYear() {
        return issueYear;
    }

    @Override
    protected String setColor() {
        return color;
    }

    @Override
    protected String setModel() {
        return super.setModel() + " Q5";
    }
}
