package AutoСatalog.Cars.Audi;

public class Audi80 extends Audi {
    /**
     * сам не понимаю почему, но без модификатора final значения не присваиваются...
     */
    private final int milage = 320_000;
    private final int issueYear = 1990;
    private final String color = "синий";


    @Override
    protected int setMilage() {
        return milage;
    }

    @Override
    protected int setIssueYear() {
        return issueYear;
    }

    @Override
    protected String setColor() {
        return color;
    }

    @Override
    protected String setModel() {
        return super.setModel() + " 80";
    }
}
