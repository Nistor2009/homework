package AutoСatalog.Cars.Audi;

public class AudiA4B8 extends Audi {
    private final int milage = 250_000;
    private final int issueYear = 2006;
    private final String color = "серый";


    @Override
    protected int setMilage() {
        return milage;
    }

    @Override
    protected int setIssueYear() {
        return issueYear;
    }

    @Override
    protected String setColor() {
        return color;
    }

    @Override
    protected String setModel() {
        return super.setModel() + " A4B8";
    }
}
