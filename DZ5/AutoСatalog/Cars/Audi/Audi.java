package AutoСatalog.Cars.Audi;

import AutoСatalog.Cars.Car;

public abstract class Audi extends Car {
    private final String MODEL = "Audi";

    @Override
    protected String setModel(){
        return MODEL;
    }
}
