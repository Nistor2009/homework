package AutoСatalog.Cars.BMW;

public class BMW5 extends BMW {
    private final int milage = 392_000;
    private final int issueYear = 2004;
    private final String color = "серый";


    @Override
    protected int setMilage() {
        return milage;
    }

    @Override
    protected int setIssueYear() {
        return issueYear;
    }

    @Override
    protected String setColor() {
        return color;
    }

    @Override
    protected String setModel() {
        return super.setModel() + " 5";
    }
}
