package AutoСatalog.Cars.BMW;

import AutoСatalog.Cars.Car;

public abstract class BMW extends Car {
    private final String MODEL = "BMW";

    @Override
    protected String setModel(){
        return MODEL;
    }
}
