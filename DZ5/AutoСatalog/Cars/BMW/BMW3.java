package AutoСatalog.Cars.BMW;

public class BMW3 extends BMW {
    private final int milage = 275_000;
    private final int issueYear = 2008;
    private final String color = "черный";


    @Override
    protected int setMilage() {
        return milage;
    }

    @Override
    protected int setIssueYear() {
        return issueYear;
    }

    @Override
    protected String setColor() {
        return color;
    }

    @Override
    protected String setModel() {
        return super.setModel() + " 3";
    }
}
