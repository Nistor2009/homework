package AutoСatalog.Cars.BMW;

public class BMWX5 extends BMW {
    private final int milage = 280_000;
    private final int issueYear = 2002;
    private final String color = "черный";


    @Override
    protected int setMilage() {
        return milage;
    }

    @Override
    protected int setIssueYear() {
        return issueYear;
    }

    @Override
    protected String setColor() {
        return color;
    }

    @Override
    protected String setModel() {
        return super.setModel() + " X5";
    }
}
