package AutoСatalog.Cars;

import AutoСatalog.Auto;

public abstract class Car extends Auto {
    private final String TYPE = "Легковой автомобиль";
    @Override
    protected String setType(){
        return TYPE;
    }
}
