package AutoСatalog.Cars.Chevrolet;

public class Camaro extends Chevrolet {
    private final int milage = 146_000;
    private final int issueYear = 2017;
    private final String color = "черный";


    @Override
    protected int setMilage() {
        return milage;
    }

    @Override
    protected int setIssueYear() {
        return issueYear;
    }

    @Override
    protected String setColor() {
        return color;
    }

    @Override
    protected String setModel() {
        return super.setModel() + " Camaro";
    }
}
