package AutoСatalog.Cars.Chevrolet;

public class Volt2 extends Chevrolet {
    private final int milage = 65_000;
    private final int issueYear = 2016;
    private final String color = "серый";


    @Override
    protected int setMilage() {
        return milage;
    }

    @Override
    protected int setIssueYear() {
        return issueYear;
    }

    @Override
    protected String setColor() {
        return color;
    }

    @Override
    protected String setModel() {
        return super.setModel() + " Volt2";
    }
}
