package AutoСatalog.Cars.Chevrolet;

import AutoСatalog.Cars.Car;

public abstract class Chevrolet extends Car {
    private final String MODEL = "Chevrolet";

    @Override
    protected String setModel(){
        return MODEL;
    }
}
