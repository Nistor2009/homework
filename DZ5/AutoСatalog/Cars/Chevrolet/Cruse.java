package AutoСatalog.Cars.Chevrolet;

public class Cruse extends Chevrolet {
    private final int milage = 116_000;
    private final int issueYear = 2013;
    private final String color = "серебристый";


    @Override
    protected int setMilage() {
        return milage;
    }

    @Override
    protected int setIssueYear() {
        return issueYear;
    }

    @Override
    protected String setColor() {
        return color;
    }

    @Override
    protected String setModel() {
        return super.setModel() + " Cruse";
    }
}
