package AutoСatalog;

public abstract class Auto {
    private int milage = setMilage();
    private int issueYear = setIssueYear();
    private String color = setColor();
    private String model = setModel();
    private String type = setType();
    private final String DESCRIPTION = String.format("%s. Марка %s. Год выпуска - %d. Пробег %d км. Цвет %s.",type,model,milage,issueYear,color);
    protected abstract int setMilage();
    protected abstract int setIssueYear();
    protected abstract String setColor();
    protected abstract String setModel();
    protected abstract String setType();
    @Override
    public String toString(){
        return DESCRIPTION;
    }
}
