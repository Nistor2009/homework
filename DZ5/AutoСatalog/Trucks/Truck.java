package AutoСatalog.Trucks;

import AutoСatalog.Auto;

public abstract class Truck extends Auto {
    private final String TYPE = "Грузовой автомобиль";
    @Override
    protected String setType(){
        return TYPE;
    }
}
