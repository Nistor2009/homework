package AutoСatalog.Trucks.Kamaz;

public class Kamaz65 extends Kamaz {
    private final int milage = 85_000;
    private final int issueYear = 2018;
    private final String color = "оранжевый";


    @Override
    protected int setMilage() {
        return milage;
    }

    @Override
    protected int setIssueYear() {
        return issueYear;
    }

    @Override
    protected String setColor() {
        return color;
    }

    @Override
    protected String setModel() {
        return super.setModel() + " 65";
    }
}
