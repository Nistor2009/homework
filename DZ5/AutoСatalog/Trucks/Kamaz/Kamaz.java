package AutoСatalog.Trucks.Kamaz;

import AutoСatalog.Trucks.Truck;

public abstract class Kamaz extends Truck {
    private final String MODEL = "Kamaz";

    @Override
    protected String setModel(){
        return MODEL;
    }
}
