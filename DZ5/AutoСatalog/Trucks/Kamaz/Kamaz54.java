package AutoСatalog.Trucks.Kamaz;

public class Kamaz54 extends Kamaz {
    private final int milage = 350_000;
    private final int issueYear = 1987;
    private final String color = "зеленый";


    @Override
    protected int setMilage() {
        return milage;
    }

    @Override
    protected int setIssueYear() {
        return issueYear;
    }

    @Override
    protected String setColor() {
        return color;
    }

    @Override
    protected String setModel() {
        return super.setModel() + " 54";
    }
}
