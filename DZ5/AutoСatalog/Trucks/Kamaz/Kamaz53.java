package AutoСatalog.Trucks.Kamaz;

public class Kamaz53 extends Kamaz{
    private final int milage = 320_000;
    private final int issueYear = 1990;
    private final String color = "желтый";


    @Override
    protected int setMilage() {
        return milage;
    }

    @Override
    protected int setIssueYear() {
        return issueYear;
    }

    @Override
    protected String setColor() {
        return color;
    }

    @Override
    protected String setModel() {
        return super.setModel() + " 53";
    }
}
