package AutoСatalog.Trucks.Maz;

import AutoСatalog.Trucks.Truck;

public abstract class Maz extends Truck {
    private final String MODEL = "Maz";

    @Override
    protected String setModel(){
        return MODEL;
    }
}
