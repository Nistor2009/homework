package AutoСatalog.Trucks.Maz;

public class Maz6501 extends Maz {
    private final int milage = 290_000;
    private final int issueYear = 2013;
    private final String color = "белый";


    @Override
    protected int setMilage() {
        return milage;
    }

    @Override
    protected int setIssueYear() {
        return issueYear;
    }

    @Override
    protected String setColor() {
        return color;
    }

    @Override
    protected String setModel() {
        return super.setModel() + " 6501";
    }
}
