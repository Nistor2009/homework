package AutoСatalog.Trucks.Maz;

public class Maz5550 extends Maz {
    private final int milage = 10_000;
    private final int issueYear = 2019;
    private final String color = "белый";


    @Override
    protected int setMilage() {
        return milage;
    }

    @Override
    protected int setIssueYear() {
        return issueYear;
    }

    @Override
    protected String setColor() {
        return color;
    }

    @Override
    protected String setModel() {
        return super.setModel() + " 5550";
    }
}
