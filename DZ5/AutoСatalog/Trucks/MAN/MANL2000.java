package AutoСatalog.Trucks.MAN;

public class MANL2000 extends MAN {
    private final int milage = 200_000;
    private final int issueYear = 1990;
    private final String color = "синий";


    @Override
    protected int setMilage() {
        return milage;
    }

    @Override
    protected int setIssueYear() {
        return issueYear;
    }

    @Override
    protected String setColor() {
        return color;
    }

    @Override
    protected String setModel() {
        return super.setModel() + " L2000";
    }
}
