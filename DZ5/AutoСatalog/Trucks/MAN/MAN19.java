package AutoСatalog.Trucks.MAN;

public class MAN19 extends MAN {
    private final int milage = 500_000;
    private final int issueYear = 1996;
    private final String color = "серый";


    @Override
    protected int setMilage() {
        return milage;
    }

    @Override
    protected int setIssueYear() {
        return issueYear;
    }

    @Override
    protected String setColor() {
        return color;
    }

    @Override
    protected String setModel() {
        return super.setModel() + " 19";
    }
}
