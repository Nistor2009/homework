package AutoСatalog.Trucks.MAN;

import AutoСatalog.Trucks.Truck;

public abstract class MAN extends Truck {
    private final String MODEL = "MAN";

    @Override
    protected String setModel(){
        return MODEL;
    }
}
