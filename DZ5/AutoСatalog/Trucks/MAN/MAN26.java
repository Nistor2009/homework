package AutoСatalog.Trucks.MAN;

public class MAN26 extends MAN {
    private final int milage = 340_000;
    private final int issueYear = 1995;
    private final String color = "желтый";


    @Override
    protected int setMilage() {
        return milage;
    }

    @Override
    protected int setIssueYear() {
        return issueYear;
    }

    @Override
    protected String setColor() {
        return color;
    }

    @Override
    protected String setModel() {
        return super.setModel() + " 26";
    }
}
