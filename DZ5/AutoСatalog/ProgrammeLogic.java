package AutoСatalog;

import AutoСatalog.Cars.Audi.Audi80;
import AutoСatalog.Cars.Audi.AudiA4B8;
import AutoСatalog.Cars.Audi.AudiQ5;
import AutoСatalog.Cars.Audi.AudiQ74L;
import AutoСatalog.Cars.BMW.BMW3;
import AutoСatalog.Cars.BMW.BMW5;
import AutoСatalog.Cars.BMW.BMWX5;
import AutoСatalog.Cars.Car;
import AutoСatalog.Cars.Chevrolet.Camaro;
import AutoСatalog.Cars.Chevrolet.Cruse;
import AutoСatalog.Cars.Chevrolet.Volt2;
import AutoСatalog.Trucks.Kamaz.Kamaz43;
import AutoСatalog.Trucks.Kamaz.Kamaz53;
import AutoСatalog.Trucks.Kamaz.Kamaz54;
import AutoСatalog.Trucks.Kamaz.Kamaz65;
import AutoСatalog.Trucks.MAN.MAN19;
import AutoСatalog.Trucks.MAN.MAN26;
import AutoСatalog.Trucks.MAN.MANL2000;
import AutoСatalog.Trucks.Maz.Maz5550;
import AutoСatalog.Trucks.Maz.Maz6501;
import AutoСatalog.Trucks.Maz.Maz6516;
import AutoСatalog.Trucks.Truck;

import java.util.Scanner;

public class ProgrammeLogic {
    private static final String FIRST_MESSAGE = "Выберите тип автомобиля:\n1 - грузовые\n2 - легковые";
    private static final String SECOND_MESSAGE = "Хотите сделать другой выбор?\n1 - главное меню\n2 - выход";
    private static final Truck[] trucks = {new Kamaz43(), new Kamaz53(), new Kamaz54(), new Kamaz65(), new MAN19(), new MAN26(), new MANL2000(), new Maz5550(), new Maz6516(), new Maz6501()};
    private static final Car[] cars = {new Audi80(), new AudiA4B8(), new AudiQ5(), new AudiQ74L(), new BMW3(), new BMW5(), new BMWX5(), new Camaro(), new Cruse(), new Volt2()};

    static int choice;    // значение от пользователя


    public static void mainMenu() {
        do {
            carListOutput();
            choice = choose(SECOND_MESSAGE);
        }while (choice==1);

    }

    private static void carListOutput() {
        choice = choose(FIRST_MESSAGE);
        if (choice == 1) {
            for (Truck truck : trucks) {
                System.out.println(truck);
            }
        }
        if (choice == 2) {
            for (Car car : cars) {
                System.out.println(car);
            }
        }
    }

    //выбор от пользователя
    private static int choose(String message) {
        int choice;     // значение от пользователя
        do {
            System.out.println(message);
            choice = checkInputValue();
        }
        while (choice != 1 && choice != 2);
        return choice;
    }

    //проверка значений, принятых от пользователя
    private static int checkInputValue() {
        Scanner scanner = new Scanner(System.in);
        boolean exitFlag = false;
        int returnValue = 0;
        while (!exitFlag) {
            exitFlag = true;
            if (scanner.hasNextInt()) {
                returnValue = scanner.nextInt();
            } else {
                exitFlag = false;
                System.out.println("Введите число");
                scanner.next();
            }
        }
        return returnValue;
    }
}
