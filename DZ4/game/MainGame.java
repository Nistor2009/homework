package game;

public class MainGame {
    public static void main(String[] args) {
        GameSimulator gameSimulator = new GameSimulator();

        /**
         * Поместил всю логику игры в GameSimulator
         */
        gameSimulator.startGame();

        // ШАГ 4
        System.out.println();
        System.out.println("Хотите сыграть еще?");
        String s = "1 - да, 0 - нет";
        System.out.println(s);
        int i = gameSimulator.checkNumber(s);
        while (!(i == 1 || i == 0)) {
            System.out.println(s);
            i = gameSimulator.checkNumber(s);
        }
        if (i == 1) {
            MainGame.main(new String[10]);
        } else {
            System.out.println("Игра завершена");
        }

    }
}
