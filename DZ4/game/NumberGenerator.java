package game;

import java.util.Random;

public class NumberGenerator {

    /**
     * долго ломал голову над этим методом. Arrays.asList(numbers).contains(j); не работает для примитивов.
     * статья по ссылке из домашки залазит в коллекции, что тоже не есть хорошо. Пришлось написать такую громоздкую конструкцию
     */


    /** NOTE! Этот метод можно сделать статическим. Так как
     * у тебя здесь только один метод, который просто генерит игроков и
     * отдает их куда-то. Твой класс не хнарит никакого состояния.
     * По сути твой метод является сервисным, который никак не
     * влияет на твою архитектуру.*/
    public static int[] generator(int size) {
        int[] numbers = new int[size];
        Random r = new Random();
        for (int i = 0; i < numbers.length; i++) {
            int j = r.nextInt(49) + 1;
            for (int k = 0; k < numbers.length; k++) {
                while (numbers[k] == j) {
                    j = r.nextInt(49) + 1;
                    k = 0;
                }
            }
            numbers[i] = j;
        }
        return numbers;

    }
}
