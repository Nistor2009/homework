package game;

import game.participants.Participant;
import game.participants.ParticipantFactory;

import java.util.Arrays;
import java.util.Scanner;

public class GameSimulator {
    private int[] winnerNumbers = new int[6];
    private Participant[] participants;
    private int participantQuantity;
    Scanner scanner = new Scanner(System.in);

    private void winnerNumberGenerator() {
        winnerNumbers = NumberGenerator.generator(6);
    }

    private void setParticipants() {
        participants = ParticipantFactory.createParticipant(participantQuantity);
    }

    public void startGame() {

        // ШАГ 1 получаем число игроков
        String s = "Введите число игроков";
        System.out.println(s);
        participantQuantity = checkNumber(s);

        // создаем игроков
        setParticipants();

        // ШАГ 2 выводим игроков на экран
        for (int i = 0; i < participants.length; i++) {
            System.out.println("Игрок " + participants[i].getParticipantNumber() + " - Числа: " + participants[i].getListToString());
        }

        // выбираем игрока
        s = "Выберите пользователя по номеру";
        System.out.println(s);
        int number = checkNumber(s);

        while (!(number <= participantQuantity && number >= 1)) {
            s = "Такого игрока нет";
            System.out.println(s);
            number = checkNumber(s);
        }
        participants[number - 1].setUserTake(true);

        // ШАГ 3 получаем выигрышную комбинацию
        winnerNumberGenerator();
        System.out.print("Выигрышная комбинация: ");
        System.out.println(Arrays.toString(winnerNumbers));

        // Определяем победителя
        determineWinner();

        // Выводим результаты
        System.out.println("Победившие игроки: ");
        for (int i = 0; i < participants.length; i++) {
            if (participants[i].isWinner()) {
                System.out.println("Игрок " + participants[i].getParticipantNumber());
            }
        }

        for (int i = 0; i < participants.length; i++) {
            if (participants[i].isUserTake()) {
                System.out.println("Ваш игрок №" + participants[i].getParticipantNumber() + ". " + (participants[number - 1].isWinner() ? "Вы выиграли!!!" : " Вы проиграли."));
            }
        }
    }


    public int checkNumber(String s) {
        while (!scanner.hasNextInt()) {
            System.out.println(s);
            scanner.next();
        }
        return scanner.nextInt();
    }

    private void determineWinner() {
        for (int i = 0; i < participants.length; i++) {
            int[] ints = participants[i].getPlayerNumbers();
            boolean firstFlag = false;
            boolean secondFlag = false;
            boolean thirdFlag = false;
            for (int j = 0; j < winnerNumbers.length; j++) {
                if (winnerNumbers[j] == ints[0]) {
                    firstFlag = true;
                }
                if (winnerNumbers[j] == ints[1]) {
                    secondFlag = true;
                }
                if (winnerNumbers[j] == ints[2]) {
                    thirdFlag = true;
                }
            }
            if (firstFlag && secondFlag && thirdFlag) {
                participants[i].setWinner(true);
            }
        }
    }

}
