package game.participants;

public class Participant {

    //private int[] list; /** NOTE! Не совсем удачное имя для поля класса */
    private int[] playerNumbers;

    private int participantNumber;
    private boolean isUserTake;
    private boolean isWinner;

    public int[] getPlayerNumbers() {
        return playerNumbers;
    }

    public Participant(int[] list, int participantNumber) {
        this.playerNumbers = list;
        this.participantNumber = participantNumber;
    }

    public String getListToString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < playerNumbers.length; i++) {
            sb.append(playerNumbers[i] + ", ");
        }
        sb.delete(sb.length() - 2, sb.length());
        return sb.toString();
    }


    public int getParticipantNumber() {
        return participantNumber;
    }

    public boolean isUserTake() {
        return isUserTake;
    }

    public void setUserTake(boolean userTake) {
        isUserTake = userTake;
    }

    public boolean isWinner() {
        return isWinner;
    }

    public void setWinner(boolean winner) {
        isWinner = winner;
    }
}
