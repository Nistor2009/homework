package game.participants;


import game.NumberGenerator;

public class ParticipantFactory {

    /** NOTE! Этот метод можно сделать статическим. Так как
     * у тебя здесь только один метод, который просто генерит игроков и
     * отдает их куда-то. Твой класс не хнарит никакого состояния.
     * По сути твой метод является сервисным, который никак не
     * влияет на твою архитектуру.*/
    public static Participant[] createParticipant(int quantity) {
        Participant[] participants = new Participant[quantity];
        for (int i = 0; i < participants.length; i++) {
            participants[i] = new Participant(NumberGenerator.generator(3), i + 1);
        }
        return participants;
    }
}
