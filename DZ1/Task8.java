/**
 * Задание № 7
 * <p>
 * 1) Даны 3 числа. Вычислить их сумму. Если все три числа равны,
 * то нужно вернуть сумму увеличеннную в два раза
 * <p>
 * 2) Даны 2 числа, нужно вернуть true,
 * если одно из низ равно 10 или их сумма равно 10
 * <p>
 * 3) Даны 3 числа, нужно вернуть максимально число
 * Результат вывести в консоль
 * <p>
 * 4) Даны 3 числа, нужно вернуть минимальное число
 * Результат вывести в консоль
 */

public class Task8 {

    public static int sum1(int a, int b, int c) {
        /**
         * NOTE! Немного хромает форматирование кода. Лучше не делать его
         * на одной и той же стороке с if или else. Лучше переносить его
         * на новую строку
         */
//        if (a == b && b == c) {
//            return (a + b + c) * 2;
//        } else return a + b + c;
        /**
         * так?
         */
        if (a == b && b == c) {
            return (a + b + c) * 2;
        } else
            return a + b + c;
    }

    public static boolean sum2(int a, int b) {
        /**
         * NOTE! Тут можно сделать в одну строчку
         */
//        if (a == 10 || b == 10 || (a + b) == 10) {
//            return true;
//        } else return false;
        /**
         *
         */
        return (a == 10 || b == 10 || (a + b) == 10);

    }

    public static int max(int a, int b, int c) {
        // TODO решение для задания №3
        if (a >= b && a >= c) {
            System.out.println(a);
            return a;

        } else if (b >= a && b >= c) {
            System.out.println(b);
            return b;
        } else {
            System.out.println(c);
            return c;
        }
    }

    public static int min(int a, int b, int c) {
        // TODO решение для задания №4
        if (a <= b && a <= c) {
            System.out.println(a);
            return a;

        } else if (b <= a && b <= c) {
            System.out.println(b);
            return b;
        } else {
            System.out.println(c);
            return c;
        }
    }

    public static void main(String[] args) {
        // вызвать функцию здесь
        System.out.println(sum1(1, 2, 2));
        System.out.println(sum2(1, 3));
        max(4, 2, 7);
        min(2, 2, 1);
    }
}
