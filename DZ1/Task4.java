/**
 * Задание № 4
 * <p>
 * 1) В переменной min лежит число от 0 до 59.
 * Определите в какую четверть часа попадает это число
 * (в первую, вторую, третью или четвертую).
 * <p>
 * - от 0 до 14 - первая
 * - от 15 до 29 - вторая
 * - от 30 до 44 - третья
 * - от 45 до 59 - четвертая
 * - если число не входит в диапазон, то вывести сообщение об этом
 * <p>
 * Пример пример сообщения:
 * System.out.println("первая")
 * <p>
 * Пример вызова функции в main:
 * checkTimePath(15)
 * checkTimePath(100)
 * <p>
 * 2) В функцию приходит время в виде миллисекунд.
 * Необходимо вывести в консолько сколько в этих миллисекундах
 * часов, минут и секунд. 1секунда = 1000 милиссекунд.
 * <p>
 * Пример вывода:
 * int hours = ...
 * System.out.println("Часы: " + hours)
 */

public class Task4 {

    public static void checkTimePath(int number) {
        // TODO написать проверку здесь и вывести сообщение
        if (number >= 0 && number <= 14) {
            System.out.println("Первая");
        } else if (number >= 15 && number <= 29) {
            System.out.println("Вторая");
        } else if (number >= 30 && number <= 44) {
            System.out.println("Третья");
        } else if (number >= 45 && number <= 59) {
            System.out.println("Четвертая");
        } else System.out.println("Вне диапазона");
    }

    public static void parseMilliseconds(long milliseconds) {
        // TODO написать деление миллисекунд
        int sec = (int) ((milliseconds / 1000) % 60);
        int min = (int) ((milliseconds / 1000 / 60) % 60);
        int hour = (int) (milliseconds / 1000 / 60 / 60);
        System.out.println("В " + milliseconds + " миллисекундах " + hour + " часов " + min + " минут и " + sec + " секунд.");
    }

    public static void main(String[] args) {
        checkTimePath(44);
        checkTimePath(80);
        parseMilliseconds(36545010);
        // вызвать функцию здесь с разными сходными параметрами
    }
}
