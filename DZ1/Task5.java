/**
 * Задание № 5
 * <p>
 * 1) Написать алгоритм округления числа до целого.
 * Например 2.5 -> 3, 2.6 -> 3, 2.4 -> 2
 * <p>
 * Вызвать функцию в методе main, а результат вывести в консоль
 * Пример вызова:
 * int result = roundNumber(4.45)
 * System.out.println(result)
 * <p>
 * 2) Написать алгоритм получения дробной части числа.
 * То есть, если у меня число 2.75, то я хочу получить 0.75
 */

public class Task5 {

    public static int roundNumber(double number) {
        /**
         * NOTE! Ты был максимально близок к идеальному решению этого
         * задания. Его можно сделать в одну строчку кода.
         * Если не поймешь как, то дай знать. Подскажу
         */

        /**
         *
         */
//        if (number % 1 >= 0.5) {
//            return ((int) number + 1);
//        } else return ((int) number);

        /**
         * как вариант через тернарный оператор. Через if-else не придумал как
         */
        return number % 1 >= 0.5 ? ((int) number + 1) : ((int) number);

    }

    public static double getTail(double number) {
        /**
         * NOTE! Для просто операции с возвращаемым значением
         * нет необходимости заводить переменную. Можно
         * прописать операцию рядом с ретурном.
         *
         * return number - (int) number;
         */
/**
 * да, уже была такая ошибка, понял
 */
//        double result = number - (int) number;
        return number - (int) number;
    }

    public static void main(String[] args) {
        int result = roundNumber(34.49);
        System.out.println(result);
        double res2 = getTail(2.75);
        System.out.println(res2);
        // вызвать функцию здесь
    }
}
