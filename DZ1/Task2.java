/**
 * Задание № 2
 * <p>
 * 1) Написать алгоритм проверки числа на четность.
 * в методе main вывести сообщение:
 * - System.out.println("Четное"), если введенное число было четным,
 * то есть функция вернула true
 * - System.out.println("Нечетное"), если введенное число было нечетным,
 * то есть функция вернула false
 * <p>
 * 2) Написать фукнцию расчета среднего числа между 4-мя значениями.
 * Результат вывести в консоль в main
 * <p>
 * 3) переделайте код метода clearOperator1() так,
 * чтобы использовались операции +=, -=, *=, /=.
 * Количество строк кода при этом не должно измениться.
 * <p>
 * 4) Переделайте этот код метода clearOperator2(),
 * чтобы в нем использовались операции ++ и --.
 * Количество строк кода при этом не должно измениться.
 * <p>
 * 5) Вычислить выражения (записать в пригодной для java форме):
 * знак "/" - дробь. Вычисления можно сделать либо отдельной функцией,
 * либо в методе main.
 * <p>
 * a) (1/4 + 5/8 - 1) * 9 - 3
 * b) 9 + 3.6 + (33/(48*5/3))
 * c) 10 * 1/2 + (48*5/3)
 */

public class Task2 {

    public static boolean isEvenNumber(int number) {
        /**
         * NOTE! По сути можно сделать только одну строку кода
         * return number % 2 == 0
         */

        /**
         * переделал
         */
        return (number % 2 == 0);
    }

    public static double getAvgNumber(int x, int y, int z, int l) {
        /**
         * NOTE! По идее явное приведение к double тут не нужно
         */

        /**
         * без явного приведения к double происходит деление int на int, дробная часть отбрасывается. У меня при делении чисел не кратных четырем результат выводит неверный
         */
        return (double) (x + y + z + l) / 4;
    }

    public static void clearOperator1() {
        int num = 47;
        num += 7;
        num -= 18;
        num *= 10;
        num /= 15;
        System.out.println("clearOperator1 " + num);
    }

    public static void clearOperator2() {
        int num = 47;
        num++;
        num--;
        num++;
        num--;
        System.out.println("clearOperator2 " + num);
    }

    public static void count() {
        /**
         * NOTE! Если хочешь привести к double, то приписывать его везде
         * не нужно, можно просто привести результат
         * (double)( 1 / 4 + 5 / 8 - 1) * 9 - 3;
         */

        /**
         * возможно где-то переборщил, но этот код получен методом проб и ошибок с проверкой на калькуляторе. Без такого приведения, опять же, исчезает дробная часть и результат получается неверныи.
         */
        double a = ((double) 1 / (double) 4 + (double) 5 / (double) 8 - 1) * 9 - 3;
        double b = 9 + 3.6 + ((double) 33 / (48 * (double) 5 / (double) 3));
        double c = 10 * 1 / 2 + (48 * 5 / 3);
        System.out.println("a = " + a);
        System.out.println("b = " + b);
        System.out.println("c = " + c);
    }

    public static void main(String[] args) {

        if (isEvenNumber(1)) {
            System.out.println("Четное");
        } else System.out.println("Нечетное");

        System.out.println("getAvgNumber 3, 7, 10, 2: " + getAvgNumber(3, 7, 10, 2));

        clearOperator1();
        clearOperator2();
        count();
    }
}
