/**
 * Задание № 7
 * <p>
 * 1) Написать функцию определения количества дней месяца месяца по его номеру.
 * То есть, в функцию будет подано число. Если это число находится
 * в диапазоне 1 - 12, то нам нужно соответствующее количество дне для месяца и
 * его название.
 * Если же было введено число, которое не попадает в заданный диапазон,
 * то нужно вывести сообщение с ошибкой ("Число не в диапазоне").
 * <p>
 * Решение должно быть представлено в двух вариантах в отдельных функциях:
 * - с использованием if-else.
 * Подсказка: можно несколько раз использовать логическое "ИЛИ"(||)
 * - с использованием switch-case
 * <p>
 * 2) Произвести вызов функций в main
 * <p>
 * Пример вызова:
 * printMonthWithIfElse(12)
 * printMonthWithIfElse(44)
 */

public class Task7 {

    public static void printDaysInMonthWithIfElse(int monthNumber) {
        if (monthNumber == 1 || monthNumber == 3 || monthNumber == 5 || monthNumber == 7 || monthNumber == 8 || monthNumber == 10 || monthNumber == 12) {
            System.out.println("31");
        } else if (monthNumber == 2) {
            System.out.println("28");
        } else if (monthNumber == 4 || monthNumber == 6 || monthNumber == 9 || monthNumber == 11) {
            System.out.println("30");
        } else System.out.println("Число не в диапазоне");
        // TODO решение через if-else
    }

    public static void printDaysInMonthWithSwithCase(int monthNumber) {
        switch (monthNumber) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                System.out.println(31);
                break;
            case 2:
                System.out.println(28);
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                System.out.println(30);
                break;
            default:
                System.out.println("Число не в диапазоне");
        }
        // TODO решение через switch-case
    }

    public static void main(String[] args) {
        // вызвать функцию здесь
        printDaysInMonthWithIfElse(13);
        printDaysInMonthWithSwithCase(2);
    }
}
