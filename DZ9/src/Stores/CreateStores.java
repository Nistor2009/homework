package Stores;

import java.util.ArrayList;
import java.util.List;

public class CreateStores {
    private CreateStores(){

    }

    public static List<Store> createStores(int downloadStores, int unloadStores){
        List<Store> list = new ArrayList<>();
        for (int i = 0; i < downloadStores; i++) {
            list.add(new DownloadStore());
        }
        for (int i = 0; i < unloadStores; i++) {
            list.add(new UnloadStore());
        }
        return list;
    }
}
