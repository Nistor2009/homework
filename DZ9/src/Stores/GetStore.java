package Stores;

import java.util.List;

public class GetStore {
    private GetStore() {

    }


    public static DownloadStore getDownloadStore(List<Store> storeList) {
        DownloadStore downloadStore = null;
        while (downloadStore == null) {
            for (Store store : storeList) {
                if ((store instanceof DownloadStore) && store.semaphore.tryAcquire()) {
                    downloadStore = (DownloadStore) store;
                    break;
                }
            }
        }
        return downloadStore;
    }

    public static UnloadStore getUnloadStore(List<Store> storeList) {
        UnloadStore unloadStore = null;
        while (unloadStore == null) {
            for (Store store : storeList) {
                if ((store instanceof UnloadStore) && store.semaphore.tryAcquire()) {
                    unloadStore = (UnloadStore) store;
                    break;
                }
            }
        }
        return unloadStore;
    }
}
