package Lorries;

import java.util.ArrayList;
import java.util.List;

public class CreateLorryPark {

private  CreateLorryPark(){

}

    public static List<Lorry> createAutoPark(int oneScope, int threeScope, int fiveScope){
        List<Lorry> list = new ArrayList<>();
        for (int i = 0; i < oneScope; i++) {
            list.add(new OneScopeLorry());
        }
        for (int i = 0; i < threeScope; i++) {
            list.add(new ThreeScopeLorry());
        }
        for (int i = 0; i < fiveScope; i++) {
            list.add(new FiveScopeLorry());
        }
        return list;
    }

}
