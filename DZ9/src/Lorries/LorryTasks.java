package Lorries;

import Stores.DownloadStore;
import Stores.GetStore;
import Stores.Store;
import Stores.UnloadStore;

import java.util.List;

public class LorryTasks implements Runnable {
    private Lorry lorry;
    private List<Store> storeList;

    public LorryTasks(Lorry lorry, List<Store> storeList) {
        this.lorry = lorry;
        this.storeList = storeList;
    }

    @Override
    public void run() {
        downloadLorry(GetStore.getDownloadStore(storeList));
        moveToStore();
        unloadLorry(GetStore.getUnloadStore(storeList));
    }

    /**
     * из-за семафора вроде уже synchronized не нужен, но я его оставил.
     */

    private void downloadLorry(DownloadStore downloadStore) {
        synchronized (downloadStore) {
        System.out.println("Грузовик " + lorry + " загружается на складе " + downloadStore);
        try {
            Thread.sleep(2000 * lorry.carrying);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        downloadStore.semaphore.release();
        }
    }

    private void moveToStore() {
        System.out.println("Грузовик " + lorry + " в пути");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void unloadLorry(UnloadStore unloadStore) {
        synchronized (unloadStore) {
        System.out.println("Грузовик " + lorry + " разгружается на складе " + unloadStore);
        try {
            Thread.sleep(1000 * lorry.carrying);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        unloadStore.semaphore.release();
        }
    }


}
