import Lorries.CreateLorryPark;
import Lorries.Lorry;
import Lorries.LorryTasks;
import Stores.CreateStores;
import Stores.Store;

import java.util.List;

public class AllLorryWorking {
    private List<Lorry> lorries;
    private List<Store> stores;

    public AllLorryWorking(int oneScope, int threeScope, int fiveScope, int downloadStores, int unloadStores) {
        lorries = CreateLorryPark.createAutoPark(oneScope, threeScope, fiveScope);
        stores = CreateStores.createStores(downloadStores, unloadStores);
    }

    public void startWork() {
        for (Lorry lorry : lorries) {
            new Thread(new LorryTasks(lorry,stores)).start();
        }
    }
}
